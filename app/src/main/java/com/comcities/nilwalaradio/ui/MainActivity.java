package com.comcities.nilwalaradio.ui;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.comcities.nilwalaradio.Helper.Utility;
import com.comcities.nilwalaradio.R;
import com.comcities.nilwalaradio.radio.IcyStreamMeta;
import com.comcities.nilwalaradio.radio.MediaPlayerService;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import io.fabric.sdk.android.Fabric;

import static android.R.attr.id;

/*
 this is main activity this load just after splash screen.
 this is the main view of the app this is main controler for app
 */
public class MainActivity extends BaseActivity implements View.OnClickListener{

    private ImageButton ibFb = null;
    private ImageButton ibTweeter = null;
    private ImageButton ibMax = null;
    private ImageButton ibMute = null;
    private ImageButton btPlay=null;
    private ImageButton ibExit = null;
    private ImageButton ibMessage = null;
    private ImageButton ibCall = null;
    private ImageButton ibEmail = null;
    private TextView tvTitle = null;
    private TextView tvArtist = null;
    private TextView tvSTitle = null;
    private SeekBar soundControler = null;
    private int initProgress = 0;
    private boolean isPlay = false;
    private AudioManager audioManager;

    private String 	streamTitle = "";
    private String 	streamArtis  = "";
    private String 	title = "";


    Intent intent = null;
    // For Extract tag
    Handler handler;
    Runnable runnable;

    RelativeLayout mainLay, grayView;
    MainActivity context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponants();
        setListners();
        //This is for start Radio
        new PlayerTask().execute(Utility.RadioStreamUrl);


    }



    private void initComponants(){

        Fabric.with(this, new Crashlytics());

        mainLay = (RelativeLayout)findViewById(R.id.activity_main);
        //This is for add the adMob

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-8467711238717343~7873606783");
        AdView mAdView = (AdView) findViewById(R.id.adView);
        //AdRequest adRequest = new AdRequest.Builder().addTestDevice("6F66CC949AD04838ADD535E77F5982DB").build();
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        // This for Allow hardware audio buttons to control volume
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        tvTitle = (TextView) findViewById(R.id.tv_Title);
        tvArtist = (TextView) findViewById(R.id.tv_artist);
        tvSTitle = (TextView) findViewById(R.id.tv_STitle);
       // mainLay = (RelativeLayout) findViewById(R.id.activity_main);
        //grayView = (RelativeLayout) findViewById(R.id.gray_view);

        tvTitle.setSelected(true);
        tvTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        tvTitle.setSingleLine(true);
        ibFb = (ImageButton) findViewById(R.id.ibFb);
        ibMax = (ImageButton) findViewById(R.id.ibMax);
        ibMute = (ImageButton) findViewById(R.id.ibMute);
        ibTweeter = (ImageButton) findViewById(R.id.ibTuwiter);
        btPlay = (ImageButton)findViewById(R.id.btPlay);
        ibMessage = (ImageButton) findViewById(R.id.ibMessage);
        ibCall = (ImageButton) findViewById(R.id.ibCall);
        ibEmail = (ImageButton) findViewById(R.id.ibEmail);

        context = this;
        if(isTablet(context)){
            btPlay.setBackgroundResource(R.drawable.btn_puse_normal_tablet);
        }else{
            btPlay.setBackgroundResource(R.drawable.btn_puse_normal_mobile);
        }

        //This is for add tweet and inicialize it
        TwitterAuthConfig authConfig =  new TwitterAuthConfig(Utility.consumerKey, Utility.consumerSecret);
        Fabric.with(this, new TwitterCore(authConfig), new TweetComposer());

        ibExit = (ImageButton) findViewById(R.id.ibExit);
        //this handeler for  synchronize the  PlayerTaskTag task.
        handler = new Handler();
        //This seekBar for controle the sound
        soundControler =(SeekBar) findViewById(R.id.sbSoundControler);

        //set seek color
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            soundControler.getProgressDrawable().setColorFilter(ContextCompat.getColor(context, R.color.seek_color), PorterDuff.Mode.SRC_IN);
        } else {
            soundControler.getProgressDrawable().setColorFilter(getResources().getColor(R.color.seek_color), PorterDuff.Mode.SRC_IN);
        }


        audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        //This sound controler for controle the device sound
        //Get the StreamMax sound and set seekBar max
        //then set device sound to initProgress and then after update the sound acodint to seekBar
        soundControler.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        initProgress = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        soundControler.setProgress(initProgress);


/*
        Bitmap bgBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.platinum_logo);
        Bitmap bluredBitMap = fastblur(bgBitmap, 0.01f, 1);


        BitmapDrawable background = new BitmapDrawable(this.getResources(), bluredBitMap);

        int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            mainLay.setBackgroundDrawable(background);
        } else {
            mainLay.setBackground(background);
        }
*/
        context = this;
        if(isTablet(context)){
            btPlay.setBackgroundResource(R.drawable.btn_puse_normal_tablet);
        }else{
            btPlay.setBackgroundResource(R.drawable.btn_puse_normal_mobile);
        }

        PlayStream();

    }

    private void setListners(){
        ibFb.setOnClickListener(this);
        ibTweeter.setOnClickListener(this);
        ibMute.setOnClickListener(this);
        ibMax.setOnClickListener(this);
        btPlay.setOnClickListener(this);
        ibMessage.setOnClickListener(this);
        ibCall.setOnClickListener(this);
        ibEmail.setOnClickListener(this);
        ibExit.setOnClickListener(this);

        PhoneStateListener phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                if (state == TelephonyManager.CALL_STATE_RINGING) {
                    //Incoming call: Pause music
                    StopPlay();
                } else if(state == TelephonyManager.CALL_STATE_IDLE) {
                    //Not in call: Play music
                    //This is for start Radio
                    PlayStream();
                } else if(state == TelephonyManager.CALL_STATE_OFFHOOK) {
                    //A call is dialing, active or on hold
                    StopPlay();
                }
                super.onCallStateChanged(state, incomingNumber);
            }
        };
        TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if(mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }

        // This is for SeekBar progress lisner for change the sound acondinto that
        soundControler.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                initProgress = progress;
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,initProgress,AudioManager.FLAG_SHOW_UI);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    //This is for All button Click Listners

    @Override
    public void onClick(View v) {


        switch (v.getId())
        {
            case R.id.ibFb :
                //Init Facebook Share
                ShareDialog shareDialog;
                FacebookSdk.sdkInitialize(context);
                shareDialog = new ShareDialog(this);
                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentTitle("Nilwala Radio")
                        .setContentDescription("Best Sinhala Radio")
                        .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.comcities.nilwalaradio")).build();
                shareDialog.show(linkContent);
                break;
            case R.id.ibExit:
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                ////
                                intent = new Intent(getApplicationContext(), MediaPlayerService.class);
                                stopService(intent);
                                isPlay = false;
                                finish();
                                System.exit(0);

                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
                builder2.setMessage("Exit FM Radio?").setPositiveButton("EXIT", dialogClickListener)
                        .setNegativeButton("CANCEL", dialogClickListener).show();


                break;
            case R.id.ibTuwiter:
                TweetComposer.Builder builder = new TweetComposer.Builder(this)
                        .text("Best sinhala radio station : https://play.google.com/store/apps/details?id=com.comcities.nilwalaradio.");
                builder.show();
                break;
            case R.id.ibEmail:
                Intent i = new Intent (Intent.ACTION_VIEW, Uri.fromParts("mailto",Utility.email, null));
                this.startActivity(i);

                break;
            case R.id.ibCall:
                if (((TelephonyManager)getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getPhoneType() == TelephonyManager.PHONE_TYPE_NONE)
                {}else {
                    Intent callIntent = new Intent(Intent.ACTION_VIEW);
                    callIntent.setData(Uri.parse(Utility.callNum));
                    startActivity(callIntent);
                }


                break;
            case R.id.ibMessage:
                //This funtion for Load default message app and fill our TP Number .
                // The number on which you want to send SMS
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", Utility.msgNum, null)));

                break;
            case R.id.ibMax :
                //This is for set volume max
                initProgress = 15;
                soundControler.setProgress(initProgress);
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,initProgress,AudioManager.ADJUST_MUTE);

                break;
            case R.id.ibMute :
                //This is for set volume mute || 0.
                initProgress = 0;
                soundControler.setProgress(initProgress);
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,initProgress,AudioManager.FLAG_SHOW_UI);

                break;
            case R.id.btPlay:
                if (isPlay){
                    StopPlay();
                }
                else {
                    PlayStream();
                }
                break;
            default:
                break;
        }
    }


    // this AsyncTask for play Radio.
    class PlayerTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            intent = new Intent(getApplicationContext(), MediaPlayerService.class);
            //Pass the parameater
            intent.putExtra(MediaPlayerService.START_PLAY, true);
            startService(intent);
            return true;
        }

        // this is start work after finish doInBackground() methord
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            //This is for start tagReader
            new PlayerTaskTag().execute();
            isPlay = true;
        }
    }

    // this AsyncTask for get meta data and set the value for textview.
    class PlayerTaskTag extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            playerMetadata();
            scheduleMetaDataUpdate();
            return true;
        }

        // this is start work after finish doInBackground() methord
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            tvTitle.setText(streamTitle);
            tvArtist.setText(streamArtis);
            tvSTitle.setText(Utility.RadioName);
        }
    }

    // this is for extract the tags
    public void playerMetadata()
    {
        try {
            IcyStreamMeta meta = new IcyStreamMeta(new URL(Utility.RadioStreamUrl));
            streamTitle = meta.getStreamTitle();
            title = meta.getTitle();
            try {
                streamArtis = meta.getArtist();
            }catch (IndexOutOfBoundsException exception){
                streamArtis = "Artist";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //This handler for loop it will call PlayerTaskTag time to time.
    public void scheduleMetaDataUpdate()
    {
        runnable = new Runnable()
        {
            @Override
            public void run()
            {
                if(isPlay)
                {
                    new PlayerTaskTag().execute();
                }
            }
        };
        handler.postDelayed(runnable, Utility.updateDillay);
    }

    private void StopPlay()
    {
        //Get new MediaPlayerService activity
        intent = new Intent(getApplicationContext(), MediaPlayerService.class);
        stopService(intent);
        tvTitle.setText("");
        tvArtist.setText("");
        tvSTitle.setText("");
        isPlay = false;

        if(isTablet(context)){
            btPlay.setBackgroundResource(R.drawable.btn_play_normal_tablet);
        }else{
            btPlay.setBackgroundResource(R.drawable.btn_play_normal_mobile);
        }

        if (runnable != null) {
            handler.removeCallbacks(runnable);
            runnable = null;
        }
    }

    private void PlayStream()
    {
        new PlayerTask().execute(Utility.RadioStreamUrl);

        if(isTablet(context)){
            btPlay.setBackgroundResource(R.drawable.btn_puse_normal_tablet);
        }else{
            btPlay.setBackgroundResource(R.drawable.btn_puse_normal_mobile);
        }
    }

}

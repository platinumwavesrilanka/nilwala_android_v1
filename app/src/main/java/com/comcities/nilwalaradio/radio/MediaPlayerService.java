package com.comcities.nilwalaradio.radio;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.comcities.nilwalaradio.Helper.Utility;
import com.comcities.nilwalaradio.R;
import com.comcities.nilwalaradio.ui.MainActivity;

import java.io.IOException;

/**
 * Created by platinumlankapvtltd on 12/28/16.
 * This is the MeadiaPlayer Service this is run as a background service
 */

public class MediaPlayerService extends Service{
    //Variables
    private boolean isPlaying = false;
    private boolean onPrepair = false;
    private MediaPlayer radioPlayer; //The media player instance
    private static int classID = 579; // just a number
    public static String START_PLAY ="START_PLAY" ;
    Object notificationObject;

    private static final String TAG = "ComcitiesService";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getBooleanExtra(START_PLAY, false)) {
            play();
        }
        return Service.START_STICKY;
    }

    /**
     * Starts radio URL stream
     */
    @SuppressLint("NewApi")
    private void play() {
        //Check connectivity status
        if (isOnline()) {
            //Check if player already streaming
            if (!isPlaying) {
                isPlaying = true;

                //Return to the current activity
                Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|
                        Intent.FLAG_ACTIVITY_SINGLE_TOP);

                PendingIntent pi = PendingIntent.getActivity(this, 0, intent, 0);

                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.JELLY_BEAN){
                    //  for JELLY_BEAN and above versions
                    //Build and show notification for radio playing
                    Notification notification = new Notification.Builder(getApplicationContext())
                            .setContentTitle(Utility.RadioName)
                            .setContentText(Utility.MainNotificationMessage)
                            .setSmallIcon(R.drawable.small_icon)
                            .setContentIntent(pi)
                            .build();
                    startForeground(classID, notification);

                } else{
                    //  before JELLY_BEAN
                    //Build and show notification for radio playing
                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                                    .setSmallIcon(R.drawable.small_icon)
                                    .setContentTitle(Utility.RadioName)
                                    .setContentText(Utility.MainNotificationMessage)
                                    .setContentIntent(pi); //Required on Gingerbread and below
                    // Gets an instance of the NotificationManager service
                    NotificationManager mNotifyMgr =
                            (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    // Builds the notification and issues it.
                    mNotifyMgr.notify(classID, mBuilder.build());

                }



                radioPlayer = new MediaPlayer();
                try {
                    radioPlayer.setDataSource(Utility.RadioStreamUrl); //StreamURL here
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (Utility.AllowConsole){
                    //Buffering Info
                    radioPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                        public void onBufferingUpdate(MediaPlayer mp, int percent) {
                            Log.i("Buffering", "" + percent);
                        }
                    });
                }

                radioPlayer.prepareAsync();
                radioPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                    public void onPrepared(MediaPlayer mp) {
                        radioPlayer.start(); //Start radio stream
                    }
                });


              //  radioPlayer.setVolume(0.01f,0.01f);



                //Display toast notification
                Toast.makeText(getApplicationContext(), Utility.PlayNotificationMessage,
                        Toast.LENGTH_LONG).show();

            }
        }
        else {
            //Display no connectivity warning
            Toast.makeText(getApplicationContext(), "No internet connection",
                    Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Stops the stream if activity destroyed
     */
    @Override
    public void onDestroy() {
        stop();
    }


    /**
     * Stops audio from the active service
     */
    private void stop() {
        if (isPlaying) {
            isPlaying = false;
            if (radioPlayer != null) {
                radioPlayer.release();
                radioPlayer = null;
            }
            stopForeground(true);
        }

        Toast.makeText(getApplicationContext(), "Stream stopped",
                Toast.LENGTH_LONG).show();
    }

    /**
     * Checks if there is a data or internet connection before starting the stream.
     * Displays Toast warning if there is no connection
     * @return online status boolean
     */
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }



}
